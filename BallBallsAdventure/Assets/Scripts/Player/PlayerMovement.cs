﻿using System;
using UnityEngine;

public class PlayerMovement : PhysicsObject
{

    public float MaxSpeed = 7;
    public float JumpTakeOffSpeed = 7;

    public SpriteRenderer[] SpriteRenderers;
    public Animator Animator;

    // Use this for initialization
    void Awake()
    {
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = JumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }

        for (int i = 0; i < SpriteRenderers.Length; i++)
        {
            var spriteRenderer = SpriteRenderers[i];
            bool flipSprite = (spriteRenderer.flipX ? (move.x < -0.01f) : (move.x > 0.01f));
            if (flipSprite)
            {
                spriteRenderer.flipX = !spriteRenderer.flipX;
            }
        }

    //    Animator.SetBool("grounded", grounded);
    //    Animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / MaxSpeed);

        targetVelocity = move * MaxSpeed;
    }
}