﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitController : MonoBehaviour
{
    public float ShrinkRate;

    public GameObject Ring;
    public GameObject Mask;

    private CircleCollider2D _circleCollider;

	// Use this for initialization
	void Start ()
	{
	    _circleCollider = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (Vector3.Distance(Ring.transform.localScale, Vector3.zero) > 0.01f)
	    {
	        Ring.transform.localScale -= Vector3.one * ShrinkRate * Time.deltaTime;
	        Mask.transform.localScale -= Vector3.one * ShrinkRate * Time.deltaTime;
	    }
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.bounds.Intersects(_circleCollider.bounds))
        {
            // Can exit level
            Debug.Log("Can exit level");
        }
    }
}
